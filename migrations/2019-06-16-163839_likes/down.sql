-- This file should undo anything in `up.sql`

DROP TABLE IF EXISTS `clients`;
DROP TABLE IF EXISTS `domains`;
DROP TABLE IF EXISTS `likes`;
DROP TABLE IF EXISTS `likes_data`;