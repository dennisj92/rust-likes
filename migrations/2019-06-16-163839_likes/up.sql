-- Your SQL goes here

# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Värd: 127.0.0.1 (MySQL 5.6.38)
# Databas: ggce-likes
# Genereringstid: 2018-06-12 12:57:52 +0000
# ************************************************************



CREATE TABLE `domains` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `domains` WRITE;
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;

INSERT INTO `domains` (`id`, `client_id`, `domain`, `created`)
VALUES
	(1,1,'chiquelle.se','2018-06-12 13:59:50'),
	(2,1,'chiquelle.com','2018-06-12 13:59:50'),
	(3,2,'johnells.se','2018-06-12 13:59:50');

/*!40000 ALTER TABLE `domains` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `likes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `client_host` varchar(64) NOT NULL DEFAULT '',
  `user_session` varchar(64) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;

INSERT INTO `likes` (`id`, `client_id`, `client_host`, `user_session`, `created`)
VALUES
	(26,1,'chiquelle.se','poi-qwe-123-987','2018-06-12 14:55:03');

/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `likes_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `like_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;