use chrono::NaiveDateTime;


struct Client {
  ID:      u64     `sql:"id"`
  Name:    String    `sql:"name"`
  Created: NaiveDateTime `sql:"created" json:"created"`
}

struct Domain {
  ID:      u64     `sql:"id"`
  Name:    String    `sql:"name"`
  Domain:  String    `sql:"domain"`
  Created: NaiveDateTime `sql:"created" json:"created"`
}

struct	Settings {
  ID:          u64     `sql:"id"`
  ClientID:    u64     `sql:"client_id" json:"client_id"`
  ClientHost:  String    `sql:"client_host" json:"client_host"`
  UserSession: String    `sql:"user_session" json:"user_session"`
  Created:     NaiveDateTime `sql:"created" json:"created"`
}

struct LikesData {
  ID:      u64       `sql:"id"`
  LikeID:  u64       `sql:"like_id" json:"like_id"`
  Key:     String      `sql:"key" json:"key"`
  Value:   interface{} `sql:"value" json:"value"`
  Created: NaiveDateTime   `sql:"created" json:"created"`
}

struct LikesGetData {
  ID:      u64     `sql:"id"`
  LikeID:  u64     `sql:"like_id" json:"like_id"`
  Key:     String    `sql:"key" json:"key"`
  Value:   []String  `sql:"value" json:"value"`
  Created: NaiveDateTime `sql:"created" json:"created"`
}

struct LikesGet {
  Settings: `json:"settings"`
  Data:     LikesGetData `json:"data"`
}

//LikesDataSlice []LikesData
struct LikePayload {
  Settings: Settings  `json:"settings"`
  Data:     LikesData `json:"data"`
}

struct DataMapLike {
  Likes:    u64 `json:"likes"`
  HasLikes: bool  `json:"has_likes"`
}