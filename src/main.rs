#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

pub mod routes;

use routes::{static_rocket_route_info_for_like, static_rocket_route_info_for_likes, static_rocket_route_info_for_revoke};

fn main() {
    rocket::ignite().mount("/", routes![like, likes, revoke]).launch();
}